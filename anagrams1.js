function onFindAnagramsClick() {
    let typedText = document.getElementById("input").value;
    showAnagrams(getAnagramsOf(typedText));
}

function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

function getAnagramsOf(typedText) {
    let sortedTypedText = alphabetize(typedText);
    let anagrams = [];
    for(var word of words) {
        if(sortedTypedText === alphabetize(word)) {
            anagrams.push(word);
        }
    }
    return anagrams;
}

function showAnagrams(anagrams) {
    for (let anagram of anagrams) {
        var node = document.createElement("span");  
        
        var textnode = document.createTextNode(anagram);
        node.appendChild(textnode);
        node.setAttribute('style', 'margin-right:3px');
        document.body.append(node); 
        
    }
}